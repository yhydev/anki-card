const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerWebpackPlugin = require('css-minimizer-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const devConfig = {
    entry: "./src/index.js",
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: "[name]-[hash].js"
    },
    plugins:[
        new HtmlWebpackPlugin(),
        new MiniCssExtractPlugin({
          filename: "[name]-[hash].css"
        })
    ],
    mode: "development",
    module: {
        rules: [
          {
            test: /\.css$/i,
            use: [MiniCssExtractPlugin.loader, "css-loader"],
          },
        ],
      },
    devServer:{
        static: {
            directory: path.resolve(__dirname, './dist'),
        },
        port: 30000
    }
}

const prodConfig = {
    entry: "./src/index.js",
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: "[name]-[hash].js"
    },
    plugins:[
      new HtmlWebpackPlugin(),
      new CssMinimizerWebpackPlugin(),
      new MiniCssExtractPlugin({
        filename: "[name]-[hash].css"
      }),
      new CopyWebpackPlugin({
        patterns:[{
          from: path.resolve(__dirname, './src/cf-pages/*'),
          to: __dirname+'/dist/[name]'
        }]
      })
    ],
    mode: "production",
    module: {
        rules: [
          {
            test: /\.css$/i,
            use: [MiniCssExtractPlugin.loader, "css-loader"],
          },
        ],
      },
    devServer:{
        static: {
            directory: path.resolve(__dirname, './dist'),
        },
        port: 30000
    }
}

module.exports = process.env.NODE_ENV == "development" ? devConfig : prodConfig